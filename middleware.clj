(ns sidekick.custom.fairtodo.middleware
  (:require [next.jdbc :as jdbc]
            [next.jdbc.sql :as sql]
            [sidekick.database.table.users-decoration :as ud]
            [sidekick.database.core :refer [datasource]]
            [sidekick.custom.fairtodo.db-fairlogin :as db-fairlogin]
            [sidekick.utils :refer [str->uuid]]
            [clojure.tools.logging :as log]))

(def debug (atom nil))

(def set-random-username
  {:name :sidekick.custom.fairtodo.middleware/set-random-username,
   :type :middleware,
   :args
     [["user-uuid-key-path"
       "e.g. [:db :users/user-uuid] looks for a user-uuid in the request map."]],
   :description "Auto sets a random username.",
   :wrap (fn [handler user-uuid-key-path]
           (fn [req]
             (jdbc/with-transaction
               [tx datasource]
               (ud/set-username tx
                                (get-in req
                                        (if (vector? user-uuid-key-path)
                                          user-uuid-key-path
                                          [user-uuid-key-path]))
                                (ud/get-random-name tx "user"))
               (handler req))))})

(def get-user-data
  {:name :sidekick.custom.fairtodo.middleware/get-user-data,
   :type :middleware,
   :description
     "Expects the fairlogin sub to be present in the query-params 'u'. Will get the user-uuid and return the every row which references the user-uuid. This middleware will not return user-data if the data is not directly referencing the user-uuid.",
   :wrap (fn [handler]
           (fn [{{sub "u"} :query-params, :as req}]
             (reset! debug req)
             (if-let [{user-uuid :fairlogin/users__uuid}
                        (sql/get-by-id datasource
                                       :fairlogin (str->uuid sub)
                                       :sub {})]
               (handler (assoc-in req
                          [:fairlogin :data-copy]
                          (db-fairlogin/get-user-data datasource user-uuid)))
               (handler req))))})

(def delete-user
  {:name :sidekick.custom.fairtodo.middleware/delete-user,
   :type :middleware,
   :description
     "Deletes the actual user by fairlogin sub found in the query-params 'u'.",
   :wrap (fn [handler]
           (fn [{{sub "u"} :query-params, :as req}]
             (reset! debug req)
             (if-let [{user-uuid :fairlogin/users__uuid}
                        (sql/get-by-id datasource
                                       :fairlogin (str->uuid sub)
                                       :sub {})]
               (if
                 (jdbc/with-transaction
                   [tx datasource]
                   (try (sql/delete! tx :fairlogin {:users__uuid user-uuid})
                        (sql/delete! tx :friend_list {:users__uuid_1 user-uuid})
                        (sql/delete! tx :friend_list {:users__uuid_2 user-uuid})
                        (doseq [{list-uuid :lists/uuid} (sql/find-by-keys
                                                          datasource
                                                          :lists
                                                          {:owner user-uuid})]
                          (sql/delete! tx :links {:lists__uuid list-uuid})
                          (sql/delete! tx :list_items {:lists__uuid list-uuid})
                          (sql/delete! tx
                                       :lists_to_users
                                       {:lists__uuid list-uuid}))
                        (sql/delete! tx
                                     :lists_to_users
                                     {:users__uuid user-uuid})
                        (sql/delete! tx :lists {:owner user-uuid})
                        (sql/delete! tx
                                     :push_notification_token
                                     {:users__uuid user-uuid})
                        (sql/delete! tx
                                     :authorization_token_bearer
                                     {:users__uuid user-uuid})
                        (sql/delete! tx :users {:uuid user-uuid})
                        true
                        (catch Exception e
                          (.rollback tx)
                          (log/error "ERROR in delete-user: " e))))
                 (handler (assoc-in req [:fairlogin :data-deletion] true))
                 (handler req))
               {:status 404,
                :header {"content-type" "text/plain"},
                :body "No User found."})))})