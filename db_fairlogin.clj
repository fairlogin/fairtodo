(ns sidekick.custom.fairtodo.db-fairlogin
  (:require [next.jdbc.sql :as sql]
            [com.rpl.specter :as sp]))

(defn get-user-data
  "Returns a map containing a data-copy."
  [ds user-uuid]
  {:users [(sql/get-by-id ds :users user-uuid :uuid {})],
   :users_decoration [(sql/get-by-id ds
                                     :users_decoration user-uuid
                                     :users__uuid {})],
   :push_notification_token [(sql/get-by-id ds
                                            :push_notification_token user-uuid
                                            :users__uuid {})],
   :roles
     (sql/query
       ds
       ["SELECT r.name, r.description FROM roles r INNER JOIN roles_to_users ru ON r.uuid = ru.roles__uuid WHERE users__uuid = ?;"
        user-uuid]),
   :lists
     (sp/transform
       [sp/ALL :list_items]
       clojure.data.json/read-str
       (sql/query
         ds
         ["\nSELECT l.*,\nCOALESCE(json_agg(li.*) FILTER (WHERE li.uuid IS NOT NULL), '[]')\n as list_items\n FROM lists l\nINNER JOIN lists_to_users lu ON l.uuid = lu.lists__uuid\nLEFT JOIN list_items li ON l.uuid = li.lists__uuid\nWHERE users__uuid = ?\nGROUP BY l.owner, l.description, l.uuid, l.title, l.created_at, l.updated_at;"
          user-uuid]))})