(ns sidekick.custom.fairtodo.db-users
  (:require [next.jdbc.sql :as sql]
            [next.jdbc :as jdbc]))

(defn get-team-uuids-by-list
  "Returns a set of user uuids how has access to a specific list."
  [ds list-uuid]
  (->> (sql/query
         ds
         ["SELECT users__uuid FROM lists_to_users WHERE lists__uuid = ?;"
          list-uuid])
       (mapv :lists_to_users/users__uuid)
       set))

(defn user-uuid->device-token
  "Returns a set of device_token if one exists for the os."
  [ds user-uuid & {:keys [os]}]
  (->>
    (if os
      (sql/query
        ds
        ["SELECT device_token FROM push_notification_token WHERE users__uuid = ? AND os = ?;"
         user-uuid os])
      (sql/query
        ds
        ["SELECT device_token FROM push_notification_token WHERE users__uuid = ?;"
         user-uuid]))
    (mapv :push_notification_token/device_token)
    set))