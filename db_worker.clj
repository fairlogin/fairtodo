(ns sidekick.custom.fairtodo.db-worker
  (:require [next.jdbc :as jdbc]
            [sidekick.utils :refer [str->uuid]]))

(defn increment-click
  [ds link-uuid]
  (jdbc/execute-one! ds
                     ["UPDATE links SET clicks = clicks + 1 WHERE uuid = ?;"
                      link-uuid]))

(defn worker-link-uuid->list
  "Resolve all needed data, once a worker requests the data."
  [ds link-uuid]
  (jdbc/execute-one!
    ds
    ["\nSELECT li.*, l.*\nFROM links li\n  INNER JOIN lists l ON li.lists__uuid = l.uuid\nWHERE li.uuid = ?;"
     link-uuid]))

(defn tick-item
  [ds list-uuid list-item-uuid]
  (jdbc/execute-one!
    ds
    ["UPDATE list_items SET state = NOT state WHERE uuid = ? AND lists__uuid = ? RETURNING *;"
     list-item-uuid list-uuid]))