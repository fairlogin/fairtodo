(ns sidekick.custom.fairtodo.handler
  (:require [next.jdbc :as jdbc]
            [next.jdbc.sql :as sql]
            [sidekick.custom.fairtodo.db-lists :as db-lists]
            [sidekick.custom.fairtodo.db-list-items :as db-list-items]
            [sidekick.custom.fairtodo.db-links :as db-links]
            [sidekick.custom.fairtodo.db-worker :as db-worker]
            [sidekick.custom.fairtodo.db-users :as db-users]
            [sidekick.database.core :refer [datasource]]
            [sidekick.database.table.authorization-token-bearer :as db-bearer]
            [sidekick.custom.fairtodo.push-notification :as push-notification]
            [sidekick.utils :refer [str->uuid]]
            [clojure.tools.logging :as log]))

(def debug (atom nil))

(def exchange-code
  {:name :sidekick.custom.fairtodo.handler/exchange-code,
   :description "Returns a the Authorization Code.",
   :type :handler,
   :handler
     (fn
       [{{bearer :authorization_token_bearer/uuid}
           :sidekick.middleware.authorization/wrap-create-authorization-token-in-request-map,
         :as req}]
       (reset! debug req)
       {:status 200,
        :headers {"content-type" "application/json"},
        :body {:bearer bearer}})})

(def get-user-data
  {:name :sidekick.custom.fairtodo.handler/get-user-data,
   :description "Returns the user data.",
   :type :handler,
   :handler
     (fn [{{{user-uuid :users/uuid} :user}
             :sidekick.middleware.authentication/identity,
           :as req}]
       (reset! debug req)
       (try
         (if-let
           [data
              (jdbc/execute-one!
                datasource
                ["SELECT ud.email, ud.users__uuid, ud.username FROM users_decoration ud WHERE ud.users__uuid = ?"
                 user-uuid])]
           {:status 200,
            :headers {"content-type" "application/json"},
            :body data}
           {:status 400,
            :headers {"content-type" "application/json"},
            :body {:en "No user data found.", :de "Keine Userdaten gefunden."}})
         (catch Exception e
           (do (log/error "ERROR in get-user-data " e)
               {:en "No user data found.",
                :de "Keine Userdaten gefunden."}))))})

(def set-user-name
  {:name :sidekick.custom.fairtodo.handler/set-user-name,
   :description "Change the user name.",
   :type :handler,
   :handler
     (fn
       [{{username :name} :body-params,
         {{user-uuid :users/uuid} :user}
           :sidekick.middleware.authentication/identity,
         :as req}]
       (reset! debug req)
       (try
         (jdbc/with-transaction
           [tx datasource]
           (sql/update! tx
                        :users_decoration
                        {:username username}
                        {:users__uuid user-uuid})
           (if-let
             [data
                (jdbc/execute-one!
                  tx
                  ["SELECT ud.email, ud.users__uuid, ud.username FROM users_decoration ud WHERE ud.users__uuid = ?"
                   user-uuid])]
             {:status 200,
              :headers {"content-type" "application/json"},
              :body data}
             {:status 400,
              :headers {"content-type" "application/json"},
              :body {:en "No user data found.",
                     :de "Keine Userdaten gefunden."}}))
         (catch Exception e
           (do (log/error "ERROR in get-user-data " e)
               {:en "Username updated failed.",
                :de "Name konnte nicht aktualisert werden."}))))})

(def get-lists
  {:name :sidekick.custom.fairtodo.handler/get-lists,
   :description "Returns every list which the user has access.",
   :type :handler,
   :handler (fn [{{{user-uuid :users/uuid} :user}
                    :sidekick.middleware.authentication/identity,
                  :as req}]
              (reset! debug req)
              (try {:status 200,
                    :headers {"content-type" "application/json"},
                    :body (or (db-lists/get-lists-by-users datasource user-uuid)
                              [])}
                   (catch Exception e
                     (do (log/error e)
                         {:status 400,
                          :headers {"content-type" "application/json"},
                          :body {:en "Could not fetch lists.",
                                 :de "Listenanfrage fehlgeschlagen."}}))))})

(def get-single-list
  {:name :sidekick.custom.fairtodo.handler/get-single-list,
   :description "Returns a single list which the user has access.",
   :type :handler,
   :handler (fn
              [{{{user-uuid :users/uuid} :user}
                  :sidekick.middleware.authentication/identity,
                {lists__uuid :uuid} :path-params,
                :as req}]
              (reset! debug req)
              (try (if-let [list (db-lists/get-single-lists-by-users
                                   datasource
                                   user-uuid
                                   (str->uuid lists__uuid))]
                     {:status 200,
                      :headers {"content-type" "application/json"},
                      :body list}
                     {:status 404,
                      :headers {"content-type" "application/json"},
                      :body {:en "No list found.",
                             :de "Keine Liste gefunden."}})
                   (catch Exception e
                     (do (log/error e)
                         {:status 400,
                          :headers {"content-type" "application/json"},
                          :body {:en "Could not fetch lists.",
                                 :de "Listenanfrage fehlgeschlagen."}}))))})

(def create-list
  {:name :sidekick.custom.fairtodo.handler/create-list,
   :description "Creates a new lists by user.",
   :type :handler,
   :handler (fn
              [{{:keys [title description]} :body-params,
                {{user-uuid :users/uuid} :user}
                  :sidekick.middleware.authentication/identity,
                :as req}]
              (reset! debug req)
              (try {:status 200,
                    :headers {"content-type" "application/json"},
                    :body (db-lists/create-by-user datasource
                                                   user-uuid
                                                   title
                                                   :description
                                                   description)}
                   (catch Exception e
                     (do (log/error e)
                         {:status 400,
                          :headers {"content-type" "application/json"},
                          :body {:en "Could not create list.",
                                 :de "Listenerstellung fehlgeschlagen."}}))))})

(def update-list
  {:name :sidekick.custom.fairtodo.handler/update-list,
   :description "Updates a lists by user.",
   :type :handler,
   :handler (fn
              [{{:keys [title description]} :body-params,
                {{user-uuid :users/uuid} :user}
                  :sidekick.middleware.authentication/identity,
                {lists__uuid :uuid} :path-params,
                :as req}]
              (reset! debug req)
              (try {:status 200,
                    :headers {"content-type" "application/json"},
                    :body (db-lists/update-by-owner datasource
                                                    (str->uuid lists__uuid)
                                                    user-uuid
                                                    :title title
                                                    :description description)}
                   (catch Exception e
                     (do (log/error e)
                         {:status 400,
                          :headers {"content-type" "application/json"},
                          :body
                            {:en "Could not update list.",
                             :de
                               "Liste konnte nicht aktualisiert werden."}}))))})

(def delete-list
  {:name :sidekick.custom.fairtodo.handler/delete-list,
   :description "Deletes a lists by user.",
   :type :handler,
   :handler (fn
              [{{{user-uuid :users/uuid} :user}
                  :sidekick.middleware.authentication/identity,
                {lists__uuid :uuid} :path-params,
                :as req}]
              (reset! debug req)
              (try (if (db-lists/delete-by-owner datasource
                                                 (str->uuid lists__uuid)
                                                 user-uuid)
                     {:status 200,
                      :headers {"content-type" "application/json"},
                      :body {:en "List was deleted successfully.",
                             :de "Liste wurde erfolgreich gelöscht."}}
                     {:status 400,
                      :headers {"content-type" "application/json"},
                      :body {:en "Could not delete list.",
                             :de "Liste konnte nicht gelöscht werden."}})
                   (catch Exception e
                     (do (log/error e)
                         {:status 400,
                          :headers {"content-type" "application/json"},
                          :body {:en "Could not delete list.",
                                 :de
                                   "Liste konnte nicht gelöscht werden."}}))))})

(def unlink-by-team
  {:name :sidekick.custom.fairtodo.handler/unlink-by-team,
   :description "Removes the user from the list.",
   :type :handler,
   :handler (fn
              [{{{user-uuid :users/uuid} :user}
                  :sidekick.middleware.authentication/identity,
                {lists__uuid :uuid} :path-params,
                :as req}]
              (reset! debug req)
              (if (try (db-lists/unlink-by-team-member datasource
                                                       (str->uuid lists__uuid)
                                                       user-uuid)
                       (catch Exception e (log/error e)))
                {:status 200,
                 :headers {"content-type" "application/json"},
                 :body {:en "List was unlinked successfully.",
                        :de "Liste wurde erfolgreich entkoppelt."}}
                {:status 400,
                 :headers {"content-type" "application/json"},
                 :body {:en "Could not unlink list.",
                        :de "Liste konnte nicht entkoppelt werden."}}))})

(def shared-user-by-owner
  {:name :sidekick.custom.fairtodo.handler/shared-user-by-owner,
   :description "Returns a list of every users, except the owner.",
   :type :handler,
   :handler (fn
              [{{{user-uuid :users/uuid} :user}
                  :sidekick.middleware.authentication/identity,
                {lists__uuid :uuid} :path-params,
                :as req}]
              (reset! debug req)
              (if-let [u (try (db-lists/get-user-by-owner datasource
                                                          (str->uuid
                                                            lists__uuid)
                                                          user-uuid)
                              (catch Exception e (log/error e)))]
                {:status 200,
                 :headers {"content-type" "application/json"},
                 :body u}
                {:status 400,
                 :headers {"content-type" "application/json"},
                 :body {:en "Could not get user list.",
                        :de "Userliste konnte nicht abgerufen werden."}}))})

(def remove-member-by-owner
  {:name :sidekick.custom.fairtodo.handler/remove-member-by-owner,
   :description "Removes access to list by the list owner.",
   :type :handler,
   :handler (fn
              [{{{owner-uuid :users/uuid} :user}
                  :sidekick.middleware.authentication/identity,
                {lists__uuid :uuid, user-uuid :user-uuid} :path-params,
                :as req}]
              (reset! debug req)
              (if-let [u (try (db-lists/delete-team-member-by-owner
                                datasource
                                (str->uuid lists__uuid)
                                (str->uuid user-uuid)
                                owner-uuid)
                              (catch Exception e (log/error e)))]
                {:status 200,
                 :headers {"content-type" "application/json"},
                 :body {:en "Member successfully deleted from list.",
                        :de "User wurde erfolgreich von der Liste entfernt."}}
                {:status 400,
                 :headers {"content-type" "application/json"},
                 :body
                   {:en "Could not delete member from list.",
                    :de "User konnte nicht von der Liste entfernt werden."}}))})

(def get-all-list-items-by-lists__uuid
  {:name :sidekick.custom.fairtodo.handler/get-all-list-items-by-lists__uuid,
   :description "Only team members are allowed to get all list items.",
   :type :handler,
   :handler (fn
              [{{{user-uuid :users/uuid} :user}
                  :sidekick.middleware.authentication/identity,
                {lists__uuid :uuid} :path-params,
                :as req}]
              (reset! debug req)
              (if-let [l (try (db-list-items/get-all-by-team datasource
                                                             (str->uuid
                                                               lists__uuid)
                                                             user-uuid)
                              (catch Exception e (log/error e)))]
                {:status 200,
                 :headers {"content-type" "application/json"},
                 :body l}
                {:status 400,
                 :headers {"content-type" "application/json"},
                 :body {:en "Could not create list item.",
                        :de "Listeneintrag konnte nicht erstellt werden."}}))})

(def create-item-by-team
  {:name :sidekick.custom.fairtodo.handler/create-item-by-team,
   :description "Only team members are allowed to create a new item.",
   :type :handler,
   :handler
     (fn
       [{{{user-uuid :users/uuid} :user}
           :sidekick.middleware.authentication/identity,
         {:keys [lists__uuid title description deadline state]} :body-params,
         :as req}]
       (reset! debug req)
       (if-let [l (try (db-list-items/create-by-team datasource
                                                     (str->uuid lists__uuid)
                                                     user-uuid
                                                     title
                                                     :description description
                                                     :deadline deadline
                                                     :state state)
                       (catch Exception e (log/error e)))]
         (let [{:users_decoration/keys [username]} (sql/get-by-id
                                                     datasource
                                                     :users_decoration user-uuid
                                                     :users__uuid {})
               set-of-user-uuid-to-notifiy (clojure.set/difference
                                             (db-users/get-team-uuids-by-list
                                               datasource
                                               (str->uuid lists__uuid))
                                             #{user-uuid})
               {l-title :lists/title} (sql/get-by-id datasource
                                                     :lists (str->uuid
                                                              lists__uuid)
                                                     :uuid {})]
           (doseq [team-uuid set-of-user-uuid-to-notifiy]
             (push-notification/send-android-push-notification-by-user-uuid!
               datasource
               :title (format "List %s updated" l-title)
               :body (format "%s added a new item '%s'" username title)
               :user-uuid team-uuid))
           {:status 200, :headers {"content-type" "application/json"}, :body l})
         {:status 400,
          :headers {"content-type" "application/json"},
          :body {:en "Could not create list item.",
                 :de "Listeneintrag konnte nicht erstellt werden."}}))})

(def update-item-by-team
  {:name :sidekick.custom.fairtodo.handler/update-item-by-team,
   :description "Only team members are allowed to update an item.",
   :type :handler,
   :handler
     (fn
       [{{{user-uuid :users/uuid} :user}
           :sidekick.middleware.authentication/identity,
         {:keys [title description]} :body-params,
         {list-item-uuid :uuid} :path-params,
         :as req}]
       (reset! debug req)
       (if-let [l (and (or title description)
                       (try
                         (db-list-items/update-by-team datasource
                                                       (str->uuid
                                                         list-item-uuid)
                                                       user-uuid
                                                       :description description
                                                       :title title)
                         (catch Exception e (log/error e))))]
         (let [{:users_decoration/keys [username]} (sql/get-by-id
                                                     datasource
                                                     :users_decoration user-uuid
                                                     :users__uuid {})
               {state :list_items/state,
                lists__uuid :list_items/lists__uuid,
                title :list_items/title}
                 (sql/get-by-id datasource
                                :list_items (str->uuid list-item-uuid)
                                :uuid {})
               set-of-user-uuid-to-notifiy (clojure.set/difference
                                             (db-users/get-team-uuids-by-list
                                               datasource
                                               (str->uuid lists__uuid))
                                             #{user-uuid})
               {l-title :lists/title} (sql/get-by-id datasource
                                                     :lists (str->uuid
                                                              lists__uuid)
                                                     :uuid {})]
           (doseq [team-uuid set-of-user-uuid-to-notifiy]
             (push-notification/send-android-push-notification-by-user-uuid!
               datasource
               :title (format "List %s updated" l-title)
               :body (format "%s edited the item '%s'" username title)
               :user-uuid team-uuid))
           {:status 200, :headers {"content-type" "application/json"}, :body l})
         {:status 400,
          :headers {"content-type" "application/json"},
          :body {:en "Could not update list item.",
                 :de "Listeneintrag konnte nicht aktualisiert werden."}}))})

(def tick-item-by-team
  {:name :sidekick.custom.fairtodo.handler/tick-item-by-team,
   :description "Only team members are allowed to tick an item.",
   :type :handler,
   :handler
     (fn
       [{{{user-uuid :users/uuid} :user}
           :sidekick.middleware.authentication/identity,
         {list-item-uuid :uuid} :path-params,
         :as req}]
       (reset! debug req)
       (if-let [l (try (db-list-items/tick-by-team datasource
                                                   (str->uuid list-item-uuid)
                                                   user-uuid)
                       (catch Exception e (log/error e)))]
         (let [{:users_decoration/keys [username]} (sql/get-by-id
                                                     datasource
                                                     :users_decoration user-uuid
                                                     :users__uuid {})
               {state :list_items/state,
                lists__uuid :list_items/lists__uuid,
                title :list_items/title}
                 (sql/get-by-id datasource
                                :list_items (str->uuid list-item-uuid)
                                :uuid {})
               set-of-user-uuid-to-notifiy (clojure.set/difference
                                             (db-users/get-team-uuids-by-list
                                               datasource
                                               (str->uuid lists__uuid))
                                             #{user-uuid})
               {l-title :lists/title} (sql/get-by-id datasource
                                                     :lists (str->uuid
                                                              lists__uuid)
                                                     :uuid {})]
           (doseq [team-uuid set-of-user-uuid-to-notifiy]
             (push-notification/send-android-push-notification-by-user-uuid!
               datasource
               :title (format "List %s updated" l-title)
               :body (format "%s added %s the item '%s'"
                             username
                             (if state "checked" "unchecked")
                             title)
               :user-uuid team-uuid))
           {:status 200, :headers {"content-type" "application/json"}, :body l})
         {:status 400,
          :headers {"content-type" "application/json"},
          :body {:en "Could not update list item.",
                 :de "Listeneintrag konnte nicht aktualisiert werden."}}))})

(def delete-item-by-team
  {:name :sidekick.custom.fairtodo.handler/delete-item-by-team,
   :description "Only team members are allowed to delete an item.",
   :type :handler,
   :handler (fn
              [{{{user-uuid :users/uuid} :user}
                  :sidekick.middleware.authentication/identity,
                {list-item-uuid :uuid} :path-params,
                :as req}]
              (reset! debug req)
              (if (try (-> (db-list-items/delete-by-team datasource
                                                         (str->uuid
                                                           list-item-uuid)
                                                         user-uuid)
                           :next.jdbc/update-count
                           (= 1))
                       (catch Exception e (log/error e)))
                {:status 200,
                 :headers {"content-type" "application/json"},
                 :body {:en "List item was deleted successfully.",
                        :de "Listeneintrag wurde erfolgreich gelöscht."}}
                {:status 400,
                 :headers {"content-type" "application/json"},
                 :body {:en "Could not delete list item.",
                        :de "Listeneintrag konnte nicht gelöscht werden."}}))})

(def create-link-by-owner
  {:name :sidekick.custom.fairtodo.handler/create-link-by-owner,
   :description "Creates a link for a list.",
   :type :handler,
   :handler (fn
              [{{{user-uuid :users/uuid} :user}
                  :sidekick.middleware.authentication/identity,
                {lists__uuid :uuid} :path-params,
                :as req}]
              (reset! debug req)
              (if-let [l (try (db-links/create-by-owner datasource
                                                        (str->uuid lists__uuid)
                                                        user-uuid)
                              (catch Exception e (log/error e)))]
                {:status 200,
                 :headers {"content-type" "application/json"},
                 :body l}
                {:status 400,
                 :headers {"content-type" "application/json"},
                 :body {:en "Could not create link.",
                        :de "Link konnte nicht erstellt werden."}}))})

(def get-links-by-owner
  {:name :sidekick.custom.fairtodo.handler/get-links-by-owner,
   :description "Returns a list of links.",
   :type :handler,
   :handler (fn
              [{{{user-uuid :users/uuid} :user}
                  :sidekick.middleware.authentication/identity,
                {lists__uuid :uuid} :path-params,
                :as req}]
              (reset! debug req)
              (if-let [l (try (or (db-links/find-by-list-uuid-by-owner
                                    datasource
                                    (str->uuid lists__uuid)
                                    user-uuid)
                                  [])
                              (catch Exception e (log/error e)))]
                {:status 200,
                 :headers {"content-type" "application/json"},
                 :body l}
                {:status 400,
                 :headers {"content-type" "application/json"},
                 :body {:en "Could not fetch link.",
                        :de "Link konnten nicht angefragt werden."}}))})

(def add-link
  {:name :sidekick.custom.fairtodo.handler/add-link,
   :description "Add a list by link.",
   :type :handler,
   :handler
     (fn
       [{{{user-uuid :users/uuid} :user}
           :sidekick.middleware.authentication/identity,
         {link-url :link-url} :body-params,
         :as req}]
       (reset! debug req)
       (try
         (if-let
           [{list-uuid :links/lists__uuid, :as l}
              (when-let
                [link-uuid
                   (str->uuid
                     (re-find
                       (re-pattern
                         "(?<=.*)[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$")
                       (or link-url "")))]
                (sidekick.database.utils/get-by-uuid datasource
                                                     :links
                                                     link-uuid))]
           (let [l (sql/insert! datasource
                                :lists_to_users
                                {:lists__uuid list-uuid,
                                 :users__uuid user-uuid})
                 {:users_decoration/keys [username]}
                   (sql/get-by-id datasource
                                  :users_decoration user-uuid
                                  :users__uuid {})
                 {l-title :lists/title}
                   (sql/get-by-id datasource :lists list-uuid :uuid {})
                 set-of-user-uuid-to-notifiy
                   (clojure.set/difference
                     (db-users/get-team-uuids-by-list datasource list-uuid)
                     #{user-uuid})]
             (doseq [team-uuid set-of-user-uuid-to-notifiy]
               (push-notification/send-android-push-notification-by-user-uuid!
                 datasource
                 :title (format "%s joined List '%s'" username l-title)
                 :body ""
                 :user-uuid team-uuid))
             {:status 200,
              :headers {"content-type" "application/json"},
              :body l})
           {:status 400,
            :headers {"content-type" "application/json"},
            :body {:en "Invalid link.", :de "Link ungültig."}})
         (catch Exception e
           (do
             (log/error e)
             {:status 400,
              :headers {"content-type" "application/json"},
              :body
                {:en "Could not add user to list.",
                 :de
                   "Der User konnte nicht zur Liste hinzugefügt werden."}}))))})

(def delete-link-by-owner
  {:name :sidekick.custom.fairtodo.handler/delete-link-by-owner,
   :description "Delete a link by the list owner.",
   :type :handler,
   :handler (fn
              [{{{user-uuid :users/uuid} :user}
                  :sidekick.middleware.authentication/identity,
                {link-uuid :uuid} :path-params,
                :as req}]
              (reset! debug req)
              (if (try (-> (db-links/delete-by-owner datasource
                                                     (str->uuid link-uuid)
                                                     user-uuid)
                           :next.jdbc/update-count
                           (= 1))
                       (catch Exception e (log/error e)))
                {:status 200,
                 :headers {"content-type" "application/json"},
                 :body {:en "Link item was deleted successfully.",
                        :de "Link wurde erfolgreich gelöscht."}}
                {:status 400,
                 :headers {"content-type" "application/json"},
                 :body {:en "Could not delete link item.",
                        :de "Link konnte nicht gelöscht werden."}}))})

(def render-worker-page
  {:name :sidekick.custom.fairtodo.handler/render-worker-page,
   :description "Renders a todo list if the link is valid?.",
   :type :handler,
   :handler
     (fn [{{link-uuid :uuid} :path-params, :as req}]
       (reset! debug req)
       (if-let [{list-uuid :lists/uuid,
                 title :lists/title,
                 description :lists/description}
                  (db-worker/worker-link-uuid->list
                    sidekick.database.core/datasource
                    (str->uuid link-uuid))]
         (do
           (when-not true
             (db-worker/increment-click datasource (str->uuid link-uuid)))
           {:status 200,
            :headers {"content-type" "text/html"},
            :body
              (selmer.util/without-escaping
                (selmer.parser/render
                  (slurp
                    "src/clj/sidekick/custom/fairtodo/resources/web/html/worker.html")
                  {:title "Fair Todo - Public List",
                   :meta
                     (format
                       "<meta http-equiv='refresh' content='5;url=/worker/%s?r=1' />"
                       link-uuid),
                   :links/uuid link-uuid,
                   :lists/title title,
                   :lists/description description,
                   :items (sort-by :list_items/title
                                   (sql/find-by-keys datasource
                                                     :list_items
                                                     {:lists__uuid
                                                        list-uuid}))}))})
         {:status 401,
          :headers {"content-type" "text/plain"},
          :body "Invalid link."}))})

(def tick-list-item-by-worker
  {:name :sidekick.custom.fairtodo.handler/tick-list-item-by-worker,
   :description
     "Ticks a todo list item by a worker, only if the link is valid.",
   :type :handler,
   :handler (fn [{{link-uuid :link-uuid, list-item-uuid :list-item-uuid}
                    :path-params,
                  :as req}]
              (reset! debug req)
              (if-let [{list-uuid :lists/uuid}
                         (db-worker/worker-link-uuid->list
                           sidekick.database.core/datasource
                           (str->uuid link-uuid))]
                (if (db-worker/tick-item datasource
                                         (str->uuid list-uuid)
                                         (str->uuid list-item-uuid))
                  ((:handler render-worker-page)
                    (assoc-in req [:path-params :uuid] link-uuid))
                  {:status 401,
                   :headers {"content-type" "text/plain"},
                   :body "User has no access."})
                {:status 401,
                 :headers {"content-type" "text/plain"},
                 :body "Invalid link."}))})

(def delete-bearer-token
  {:name :sidekick.custom.fairtodo.handler/delete-bearer-token,
   :description "Removes all bearer tokens and device tokens by user",
   :type :handler,
   :handler (fn [{{{user-uuid :users/uuid} :user}
                    :sidekick.middleware.authentication/identity,
                  :as req}]
              (reset! debug req)
              (jdbc/with-transaction
                [tx datasource]
                (try (db-bearer/remove-all-by-user-uuid tx user-uuid)
                     (sql/delete! tx
                                  :push_notification_token
                                  {:users__uuid user-uuid})
                     {:status 200,
                      :headers {"content-type" "application/json"},
                      :body {:en "Token deleted successfully.",
                             :de "Token erfolgreich gelöscht."}}
                     (catch Exception e
                       (do (.rollback tx)
                           {:status 400,
                            :headers {"content-type" "text/plain"},
                            :body "Could not delete bearer token."})))))})

(def add-new-push-token
  {:name :sidekick.custom.fairtodo.handler/add-new-push-token,
   :description
     "Takes a new device token and puts it in the push_notification_table.",
   :type :handler,
   :handler (fn
              [{{{user-uuid :users/uuid} :user}
                  :sidekick.middleware.authentication/identity,
                {:keys [device_token os]} :body-params,
                :as req}]
              (reset! debug req)
              (try (sql/insert! datasource
                                :push_notification_token
                                {:device_token device_token,
                                 :os (clojure.string/lower-case os),
                                 :users__uuid user-uuid}
                                {:suffix "ON CONFLICT DO NOTHING"})
                   {:status 200,
                    :headers {"content-type" "application/json"},
                    :body {:en "Token updated successfully.",
                           :de "Token erfolgreich aktualisert."}}
                   (catch Exception e
                     {:status 400,
                      :headers {"content-type" "application/json"},
                      :body {:en "Could not add device token.",
                             :de "Token konnte nicht hinzugefügt werden."}})))})