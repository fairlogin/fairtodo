(ns sidekick.custom.fairtodo.db-lists
  (:require [next.jdbc :as jdbc]
            [next.jdbc.sql :as sql]
            [clojure.tools.logging :as log]
            [sidekick.database.utils :refer [get-by-uuid]]
            [sidekick.utils :refer [remove-nil-keys]]
            [clojure.spec.alpha :as s]))

(defn get-lists-by-users
  [ds user-uuid]
  (when (s/valid? uuid? user-uuid)
    (jdbc/execute!
      ds
      ["SELECT *\nFROM lists l INNER JOIN lists_to_users lu ON l.uuid = lu.lists__uuid\nWHERE lu.users__uuid = ?;"
       user-uuid])))

(defn get-single-lists-by-users
  [ds user-uuid list-uuid]
  (when (and (s/valid? uuid? user-uuid) (s/valid? uuid? list-uuid))
    (jdbc/execute-one!
      ds
      ["SELECT *\nFROM lists l INNER JOIN lists_to_users lu ON l.uuid = lu.lists__uuid\nWHERE lu.users__uuid = ? AND l.uuid = ?;"
       user-uuid list-uuid])))

(defn create-by-user
  [ds user-uuid title & {desc :description}]
  (jdbc/with-transaction
    [tx ds]
    (try (let [{list-uuid :lists/uuid, :as l} (sql/insert! ds
                                                           :lists
                                                           {:title title,
                                                            :description desc,
                                                            :owner user-uuid})]
           (sql/insert! ds
                        :lists_to_users
                        {:lists__uuid list-uuid, :users__uuid user-uuid})
           l)
         (catch Exception e (do (.rollback tx) (throw e))))))

(defn update-by-owner
  [ds list-uuid owner-uuid & {title :title, desc :description}]
  (do (sql/update! ds
                   :lists
                   (remove-nil-keys {:title title, :description desc})
                   {:uuid list-uuid, :owner owner-uuid})
      (get-by-uuid ds :lists list-uuid)))

(defn delete-by-owner
  [ds list-uuid owner-uuid]
  (if
    (:bool
      (jdbc/execute-one!
        ds
        ["\nSELECT true\nFROM lists l\n  INNER JOIN lists_to_users lu ON l.uuid = lu.lists__uuid\nWHERE l.uuid = ? AND l.owner = ?;"
         list-uuid owner-uuid]
        {:return-keys true}))
    (jdbc/with-transaction
      [tx ds]
      (try (sql/delete! tx :list_items {:lists__uuid list-uuid})
           (sql/delete! tx :lists_to_users {:lists__uuid list-uuid})
           (sql/delete! tx :links {:lists__uuid list-uuid})
           (when-not (-> (sql/delete! tx
                                      :lists
                                      {:uuid list-uuid, :owner owner-uuid})
                         :next.jdbc/update-count
                         pos?)
             (.rollback tx)
             (throw (Exception. "No list found.")))
           true
           (catch Exception e
             (do (log/error e)
                 (.rollback tx)
                 (throw (Exception. "Could not delete list."))))))
    (throw (Exception. "User is not authorized to perform this action."))))

(defn get-user-by-owner
  [ds list-uuid user-uuid]
  (jdbc/execute!
    ds
    ["SELECT u.uuid, ud.username FROM users u\nINNER JOIN users_decoration ud ON ud.users__uuid = u.uuid\nINNER JOIN lists_to_users lu ON u.uuid = lu.users__uuid\nINNER JOIN lists l ON l.uuid = lu.lists__uuid\nWHERE l.uuid = ? AND l.owner = ? AND u.uuid != ?;"
     list-uuid user-uuid user-uuid]))

(defn delete-team-member-by-owner
  [ds list-uuid user-uuid owner-uuid]
  (if
    (:bool
      (jdbc/execute-one!
        ds
        ["\nSELECT true\nFROM lists l\n  INNER JOIN lists_to_users lu ON l.uuid = lu.lists__uuid\nWHERE l.uuid = ? AND l.owner = ? AND l.owner != ?;"
         list-uuid owner-uuid user-uuid]
        {:return-keys true}))
    (jdbc/execute!
      ds
      ["DELETE FROM lists_to_users WHERE lists__uuid = ? AND users__uuid = ?;"
       list-uuid user-uuid])
    (throw (Exception. "User is not authorized to perform this action."))))

(defn unlink-by-team-member
  [ds list-uuid user-uuid]
  (if
    (:bool
      (jdbc/execute-one!
        ds
        ["\nSELECT true\nFROM lists l\n  INNER JOIN lists_to_users lu ON l.uuid = lu.lists__uuid\nWHERE l.uuid = ? AND lu.users__uuid = ? AND l.owner != ?;"
         list-uuid user-uuid user-uuid]
        {:return-keys true}))
    (jdbc/execute!
      ds
      ["DELETE FROM lists_to_users WHERE lists__uuid = ? AND users__uuid = ?;"
       list-uuid user-uuid])
    (throw (Exception. "User is not authorized to perform this action."))))