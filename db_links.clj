(ns sidekick.custom.fairtodo.db-links
  (:require [next.jdbc :as jdbc]
            [next.jdbc.sql :as sql]
            [clojure.spec.alpha :as s]))

(defn create-by-owner
  [ds list-uuid user-uuid]
  (when (and (s/valid? uuid? user-uuid) (s/valid? uuid? list-uuid))
    (if (:bool (jdbc/execute-one!
                 ds
                 ["SELECT true FROM lists l WHERE l.uuid = ? AND l.owner = ?;"
                  list-uuid user-uuid]
                 {:return-keys true}))
      (sql/insert! ds :links {:lists__uuid list-uuid})
      (throw (Exception. "User is not authorized to perform this action.")))))

(defn delete-by-owner
  [ds link-uuid user-uuid]
  (when (and (s/valid? uuid? user-uuid) (s/valid? uuid? link-uuid))
    (if
      (:bool
        (jdbc/execute-one!
          ds
          ["\nSELECT true\nFROM links li\n  INNER JOIN lists l ON li.lists__uuid = l.uuid\nWHERE li.uuid = ? AND l.owner = ?;"
           link-uuid user-uuid]
          {:return-keys true}))
      (sql/delete! ds :links {:uuid link-uuid})
      (throw (Exception. "User is not authorized to perform this action.")))))

(defn find-by-list-uuid-by-owner
  [ds list-uuid user-uuid]
  (when (and (s/valid? uuid? user-uuid) (s/valid? uuid? list-uuid))
    (if (:bool (jdbc/execute-one!
                 ds
                 ["SELECT true FROM lists l WHERE l.uuid = ? AND l.owner = ?;"
                  list-uuid user-uuid]
                 {:return-keys true}))
      (first (sql/query ds
                        ["SELECT * FROM links WHERE lists__uuid = ? LIMIT 1;"
                         list-uuid]))
      (throw (Exception. "User is not authorized to perform this action.")))))

(defn worker-link-uuid->list
  "Resolve all needed data, once a worker requests the data."
  [ds link-uuid]
  (jdbc/execute-one!
    ds
    ["\nSELECT li.*, l.*\nFROM links li\n  INNER JOIN lists l ON li.lists__uuid = l.uuid\nWHERE li.uuid = ?;"
     link-uuid]))