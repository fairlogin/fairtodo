(ns sidekick.custom.fairtodo.push-notification
  (:require [sidekick.config :refer [env]]
            [clojure.data.json :as json]
            [sidekick.custom.fairtodo.db-users :refer [user-uuid->device-token]]
            [org.httpkit.client :refer [post]]))

(defn send-android-push-notification!
  "Sends a push notification to a single user by device token.\n  Title - Notification Title, Body - Notification Body"
  [& {:keys [title body device-token]}]
  (clojure.core/deref (post
                        "https://fcm.googleapis.com/fcm/send"
                        {:headers {"content-type" "application/json",
                                   "Authorization"
                                     (str "key="
                                          (-> env
                                              :fairtodo
                                              :firebase-cloud-messaging-key))},
                         :body (json/write-str
                                 {:to device-token,
                                  :notification {:sound "default",
                                                 :body body,
                                                 :title title,
                                                 :content_available true},
                                  :data {:sound "default",
                                         :body body,
                                         :title title,
                                         :content_available true}})})))

(defn send-android-push-notification-by-user-uuid!
  [ds & {:keys [title body user-uuid]}]
  (doseq [device-token (user-uuid->device-token ds user-uuid :os "android")]
    (future (send-android-push-notification! :title title
                                             :body body
                                             :device-token device-token))))