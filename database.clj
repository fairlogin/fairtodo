(ns sidekick.custom.fairtodo.database)

(def database-configuration
  {:description "Database config for fairtodo extension.",
   :custom-endpoints
     [{:description
         "Gets a Authorization Code from Fair Login and return an Authorization Bearer.",
       :method "post",
       :admin_route false,
       :name "Fair Todo Exchange Code",
       :roles ["Public"],
       :custom true,
       :route "/fairlogin/bearer/:code",
       :middleware
         {:middleware-stack
            [["sidekick.middleware.response/wrap-replace-error"
              "{:en \"Could not log user in.\" :de \"Anmeldung fehlgeschlagen.\"}"]
             ["sidekick.middleware.formats/wrap-path-params->query-params"
              ":code"]
             ["sidekick.custom.fairlogin.middleware/openid-authorization-code-flow"]
             ["sidekick.custom.fairlogin.middleware/login-user"]
             ["sidekick.custom.web.middleware/set-random-username"
              "[:fairlogin :db :users :users/uuid]"]
             ["sidekick.middleware.authorization/wrap-create-authorization-token-in-request-map"
              "[:fairlogin :db :users :users/uuid]"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key "sidekick.custom.fairtodo.handler/exchange-code"}}
      {:description "Returns the user data including username, email.",
       :method "get",
       :admin_route false,
       :name "Fair Todo get User Data",
       :roles ["User"],
       :custom true,
       :route "/users/account",
       :middleware
         {:middleware-stack
            [["sidekick.middleware.formats/wrap-unqualify-keys"]
             ["sidekick.middleware.response/wrap-replace-error"
              "{:en \"No User Data found.\" :de \"Keine Userdaten gefunden.\"}"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key "sidekick.custom.fairtodo.handler/get-user-data"}}
      {:description "Updates the username.",
       :method "post",
       :admin_route false,
       :name "Fair Todo update Username",
       :roles ["User"],
       :custom true,
       :route "/users/account/name",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key "sidekick.custom.fairtodo.handler/set-user-name"}}
      {:description "Returns every list which the user has access.",
       :method "get",
       :admin_route false,
       :name "Fair Todo get lists",
       :roles ["User"],
       :custom true,
       :route "/users/lists",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key "sidekick.custom.fairtodo.handler/get-lists"}}
      {:description "Returns a single list which the user has access.",
       :method "get",
       :admin_route false,
       :name "Fair Todo get single list",
       :roles ["User"],
       :custom true,
       :route "/users/lists/:uuid",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key
                   "sidekick.custom.fairtodo.handler/get-single-list"}}
      {:description "Creates a single list by user.",
       :method "post",
       :admin_route false,
       :name "Fair Todo create single list",
       :roles ["User"],
       :custom true,
       :route "/users/lists",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key "sidekick.custom.fairtodo.handler/create-list"}}
      {:description "Updates a lists by user.",
       :method "put",
       :admin_route false,
       :name "Fair Todo update single list",
       :roles ["User"],
       :custom true,
       :route "/users/lists/:uuid",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key "sidekick.custom.fairtodo.handler/update-list"}}
      {:description "Deletes a lists by user.",
       :method "delete",
       :admin_route false,
       :name "Fair Todo deletes single list",
       :roles ["User"],
       :custom true,
       :route "/users/lists/:uuid",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key "sidekick.custom.fairtodo.handler/delete-list"}}
      {:description "Removes the user from the list.",
       :method "delete",
       :admin_route false,
       :name "Fair Todo unlink from single list",
       :roles ["User"],
       :custom true,
       :route "/users/lists/unlink/:uuid",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key
                   "sidekick.custom.fairtodo.handler/unlink-by-team"}}
      {:description "Returns a list of every users, except the owner.",
       :method "get",
       :admin_route false,
       :name "Fair Todo get team",
       :roles ["User"],
       :custom true,
       :route "/users/team-list/:uuid",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key
                   "sidekick.custom.fairtodo.handler/shared-user-by-owner"}}
      {:description "Removes access to list by the list owner.",
       :method "delete",
       :admin_route false,
       :name "Fair Todo remove team member",
       :roles ["User"],
       :custom true,
       :route "/users/team-list/:uuid/:user-uuid",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key
                   "sidekick.custom.fairtodo.handler/remove-member-by-owner"}}
      {:description
         "Get all list items by list only if the user has access to the parent list.",
       :method "get",
       :admin_route false,
       :name "Fair Todo get list items by team member",
       :roles ["User"],
       :custom true,
       :route "/users/list_items/all/:uuid",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler
         {:handler-key
            "sidekick.custom.fairtodo.handler/get-all-list-items-by-list-uuid"}}
      {:description
         "Get create list items only if the user has access to the parent list.",
       :method "post",
       :admin_route false,
       :name "Fair Todo create list item by team member",
       :roles ["User"],
       :custom true,
       :route "/users/list_items",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key
                   "sidekick.custom.fairtodo.handler/create-item-by-team"}}
      {:description
         "Update list items only if the user has access to the parent list.",
       :method "put",
       :admin_route false,
       :name "Fair Todo update list item by team member",
       :roles ["User"],
       :custom true,
       :route "/users/list_items/:uuid",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key
                   "sidekick.custom.fairtodo.handler/update-item-by-team"}}
      {:description
         "Tick list item TODO -> DONE -> TODO only if the user has access to the parent list.",
       :method "post",
       :admin_route false,
       :name "Fair Todo tick list item by team member",
       :roles ["User"],
       :custom true,
       :route "/users/list_items/:uuid/tick",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key
                   "sidekick.custom.fairtodo.handler/tick-item-by-team"}}
      {:description
         "Delete list item only if the user has access to the parent list.",
       :method "delete",
       :admin_route false,
       :name "Fair Todo delete list item by team member",
       :roles ["User"],
       :custom true,
       :route "/users/list_items/:uuid",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key
                   "sidekick.custom.fairtodo.handler/delete-item-by-team"}}
      {:description "Creates a link for a list.",
       :method "post",
       :admin_route false,
       :name "Fair Todo create link",
       :roles ["User"],
       :custom true,
       :route "/users/link/:uuid",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key
                   "sidekick.custom.fairtodo.handler/create-link-by-owner"}}
      {:description "Returns a list of links.",
       :method "get",
       :admin_route false,
       :name "Fair Todo get links",
       :roles ["User"],
       :custom true,
       :route "/users/link/by-list/:uuid",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key
                   "sidekick.custom.fairtodo.handler/get-links-by-owner"}}
      {:description "Delete a link by the list owner.",
       :method "delete",
       :admin_route false,
       :name "Fair Todo delete link",
       :roles ["User"],
       :custom true,
       :route "/users/link/:uuid",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key
                   "sidekick.custom.fairtodo.handler/delete-link-by-owner"}}
      {:description "Add a list by link.",
       :method "post",
       :admin_route false,
       :name "Fair Todo add link",
       :roles ["User"],
       :custom true,
       :route "/users/link-add",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key "sidekick.custom.fairtodo.handler/add-link"}}
      {:description "Renders the worker page.",
       :method "get",
       :admin_route false,
       :name "Fair Todo Render Worker page",
       :roles ["Public"],
       :custom true,
       :route "/worker/:uuid",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key
                   "sidekick.custom.fairtodo.handler/render-worker-page"}}
      {:description
         "Ticks an list item by a worker, renders the page afterwards.",
       :method "get",
       :admin_route false,
       :name "Fair Todo Tick Item By Worker",
       :roles ["Public"],
       :custom true,
       :route "/worker/:link-uuid/tick/:list-item-uuid",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key
                   "sidekick.custom.fairtodo.handler/tick-list-item-by-worker"}}
      {:description "Unlinks the user from every device.",
       :method "delete",
       :admin_route false,
       :name "Fair Todo Remove Bearer",
       :roles ["User"],
       :custom true,
       :route "/users/unlink",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key
                   "sidekick.custom.fairtodo.handler/delete-bearer-token"}}
      {:description "Adds a device token for push notifications.",
       :method "post",
       :admin_route false,
       :name "Fair Todo Add Device Token",
       :roles ["User"],
       :custom true,
       :route "/users/add-device-token",
       :middleware {:middleware-stack
                      [["sidekick.middleware.formats/wrap-unqualify-keys"]]},
       :active true,
       :table_name "fairtodo",
       :handler {:handler-key
                   "sidekick.custom.fairtodo.handler/add-new-push-token"}}],
   :create
     ["CREATE TABLE IF NOT EXISTS friend_list (users__uuid_1 UUID NOT NULL REFERENCES users(uuid),users__uuid_2 UUID NOT NULL REFERENCES users(uuid),created_at TIMESTAMP DEFAULT NOW() NOT NULL,updated_at TIMESTAMP DEFAULT NOW() NOT NULL,PRIMARY KEY (users__uuid_1,users__uuid_2));"
      "CREATE TABLE IF NOT EXISTS lists (owner UUID NOT NULL REFERENCES users(uuid),description TEXT,uuid UUID DEFAULT uuid_generate_v4(),title TEXT NOT NULL,created_at TIMESTAMP DEFAULT NOW() NOT NULL,updated_at TIMESTAMP DEFAULT NOW() NOT NULL,PRIMARY KEY (uuid));"
      "CREATE TABLE IF NOT EXISTS lists_to_users (lists__uuid UUID NOT NULL REFERENCES lists(uuid),users__uuid UUID NOT NULL REFERENCES users(uuid),created_at TIMESTAMP DEFAULT NOW() NOT NULL,updated_at TIMESTAMP DEFAULT NOW() NOT NULL);"
      "CREATE TABLE IF NOT EXISTS list_items (lists__uuid UUID NOT NULL REFERENCES lists(uuid),state BOOLEAN NOT NULL,uuid UUID DEFAULT uuid_generate_v4(),description TEXT,deadline TIMESTAMP,title TEXT NOT NULL,created_at TIMESTAMP DEFAULT NOW() NOT NULL,updated_at TIMESTAMP DEFAULT NOW() NOT NULL,PRIMARY KEY (uuid));"
      "CREATE TABLE IF NOT EXISTS links (lists__uuid UUID REFERENCES lists(uuid),delete_at TIMESTAMP,uuid UUID DEFAULT uuid_generate_v4(),clicks INTEGER DEFAULT 0,created_at TIMESTAMP DEFAULT NOW() NOT NULL,updated_at TIMESTAMP DEFAULT NOW() NOT NULL,PRIMARY KEY (uuid), UNIQUE(lists__uuid));"
      "CREATE TABLE IF NOT EXISTS products (Art TEXT,uuid UUID DEFAULT uuid_generate_v4(),created_at TIMESTAMP DEFAULT NOW() NOT NULL,updated_at TIMESTAMP DEFAULT NOW() NOT NULL,PRIMARY KEY (uuid));"
      "CREATE TABLE IF NOT EXISTS push_notification_token (users__uuid UUID REFERENCES users(uuid),os TEXT NOT NULL,uuid UUID DEFAULT uuid_generate_v4(),device_token TEXT NOT NULL,created_at TIMESTAMP DEFAULT NOW() NOT NULL,updated_at TIMESTAMP DEFAULT NOW() NOT NULL,PRIMARY KEY (uuid),UNIQUE (device_token));"],
   :name :fairtodo,
   :roles
     [{:name "User",
       :active true,
       :description
         "Every Fair Todo Customer must be part of the User Role. The User role allows to create Lists, Items, Links and to edit the own Profile."}
      {:name "Support",
       :active false,
       :description "This role is only for the Fair Todo Support Team."}],
   :drop ["DROP TABLE IF EXISTS friend_list CASCADE;"
          "DROP TABLE IF EXISTS lists CASCADE;"
          "DROP TABLE IF EXISTS lists_to_users CASCADE;"
          "DROP TABLE IF EXISTS list_items CASCADE;"
          "DROP TABLE IF EXISTS links CASCADE;"
          "DROP TABLE IF EXISTS products CASCADE;"
          "DROP TABLE IF EXISTS push_notification_token CASCADE;"],
   :type :database,
   :rest-endpoints
     [{:endpoint-merge
         [{:name "count",
           :middleware
             {:middleware-stack
                [["sidekick.middleware.tables/wrap-inject-table-name" "lists"]
                 ["sidekick.middleware.formats/wrap-unqualify-keys"]]},
           :active true,
           :roles ["User"]}],
       :table_name "lists"}],
   :trigger
     ["DROP TRIGGER IF EXISTS update_friend_list_updated_at ON friend_list; CREATE TRIGGER update_friend_list_updated_at BEFORE UPDATE ON friend_list FOR EACH ROW EXECUTE PROCEDURE updated_at_trigger();"
      "DROP TRIGGER IF EXISTS update_lists_updated_at ON lists; CREATE TRIGGER update_lists_updated_at BEFORE UPDATE ON lists FOR EACH ROW EXECUTE PROCEDURE updated_at_trigger();"
      "DROP TRIGGER IF EXISTS update_lists_to_users_updated_at ON lists_to_users; CREATE TRIGGER update_lists_to_users_updated_at BEFORE UPDATE ON lists_to_users FOR EACH ROW EXECUTE PROCEDURE updated_at_trigger();"
      "DROP TRIGGER IF EXISTS update_list_items_updated_at ON list_items; CREATE TRIGGER update_list_items_updated_at BEFORE UPDATE ON list_items FOR EACH ROW EXECUTE PROCEDURE updated_at_trigger();"
      "DROP TRIGGER IF EXISTS update_links_updated_at ON links; CREATE TRIGGER update_links_updated_at BEFORE UPDATE ON links FOR EACH ROW EXECUTE PROCEDURE updated_at_trigger();"
      "DROP TRIGGER IF EXISTS update_products_updated_at ON products; CREATE TRIGGER update_products_updated_at BEFORE UPDATE ON products FOR EACH ROW EXECUTE PROCEDURE updated_at_trigger();"
      "DROP TRIGGER IF EXISTS update_push_notification_token_updated_at ON push_notification_token; CREATE TRIGGER update_push_notification_token_updated_at BEFORE UPDATE ON push_notification_token FOR EACH ROW EXECUTE PROCEDURE updated_at_trigger();"]})