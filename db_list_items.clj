(ns sidekick.custom.fairtodo.db-list-items
  (:require [next.jdbc :as jdbc]
            [next.jdbc.sql :as sql]
            [clojure.tools.logging :as log]
            [sidekick.database.utils :refer [get-by-uuid]]
            [sidekick.utils :refer [remove-nil-keys dissoc-namespace]]
            [clojure.spec.alpha :as s]))

(defn get-all-by-team
  [ds list-uuid user-uuid]
  (when (s/valid? uuid? user-uuid)
    (->>
      (jdbc/execute!
        ds
        ["\nSELECT *\nFROM list_items li\n     INNER JOIN lists l\n       ON li.lists__uuid = l.uuid\n     INNER JOIN lists_to_users lu\n       ON l.uuid = lu.lists__uuid\nWHERE lu.users__uuid = ? AND lu.lists__uuid = ?;"
         user-uuid list-uuid])
      (mapv (comp (fn* [p1__50065#] (dissoc-namespace p1__50065# "lists"))
                  (fn* [p1__50066#]
                    (dissoc-namespace p1__50066# "lists_to_users")))))))

(defn create-by-team
  [ds list-uuid user-uuid title &
   {deadline :deadline, desc :description, state :state}]
  (when (and (s/valid? uuid? user-uuid) (s/valid? uuid? list-uuid))
    (if
      (jdbc/execute-one!
        ds
        ["\nSELECT * FROM lists_to_users lu WHERE lu.users__uuid = ? AND lu.lists__uuid = ?;"
         user-uuid list-uuid])
      (do
        (jdbc/execute-one!
          ds
          [(format
             "\nINSERT INTO list_items (lists__uuid, title, state, deadline, description)\n VALUES (?,?,'%s',?,?)\nRETURNING *;"
             (boolean state)) list-uuid title deadline desc]))
      (throw (Exception. "User is not authorized to perform this action.")))))

(defn update-by-team
  "No ticking. Nil values will be ignored."
  [ds list-item-uuid user-uuid & {title :title, desc :description}]
  (when (and (s/valid? uuid? user-uuid) (s/valid? uuid? list-item-uuid))
    (if
      (:bool
        (jdbc/execute-one!
          ds
          ["\nSELECT true\nFROM list_items li\n  INNER JOIN lists l ON li.lists__uuid = l.uuid\n  INNER JOIN lists_to_users lu ON l.uuid = lu.lists__uuid\nWHERE lu.users__uuid = ? AND li.uuid = ?;"
           user-uuid list-item-uuid]
          {:return-keys true}))
      (do (sql/update! ds
                       :list_items
                       (remove-nil-keys {:title title, :description desc})
                       {:uuid list-item-uuid})
          (get-by-uuid ds :list_items list-item-uuid))
      (throw (Exception. "User is not authorized to perform this action.")))))

(defn tick-by-team
  "Consider only DONE and TODO states at this point."
  [ds list-item-uuid user-uuid]
  (when (and (s/valid? uuid? user-uuid) (s/valid? uuid? list-item-uuid))
    (if
      (:bool
        (jdbc/execute-one!
          ds
          ["\nSELECT true\nFROM list_items li\n  INNER JOIN lists l ON li.lists__uuid = l.uuid\n  INNER JOIN lists_to_users lu ON l.uuid = lu.lists__uuid\nWHERE lu.users__uuid = ? AND li.uuid = ?;"
           user-uuid list-item-uuid]
          {:return-keys true}))
      (let [{state :list_items/state} (get-by-uuid ds
                                                   :list_items
                                                   list-item-uuid)]
        (jdbc/execute-one!
          ds
          [(format
             "UPDATE list_items SET state = NOT state WHERE uuid = ? RETURNING *;")
           list-item-uuid]))
      (throw (Exception. "User is not authorized to perform this action.")))))

(defn delete-by-team
  [ds list-item-uuid user-uuid]
  (when (and (s/valid? uuid? user-uuid) (s/valid? uuid? list-item-uuid))
    (if
      (:bool
        (jdbc/execute-one!
          ds
          ["\nSELECT true\nFROM list_items li\n  INNER JOIN lists l ON li.lists__uuid = l.uuid\n  INNER JOIN lists_to_users lu ON l.uuid = lu.lists__uuid\nWHERE lu.users__uuid = ? AND li.uuid = ?;"
           user-uuid list-item-uuid]
          {:return-keys true}))
      (sql/delete! ds :list_items {:uuid list-item-uuid})
      (throw (Exception. "User is not authorized to perform this action.")))))